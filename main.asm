%include "lib.inc"
%include "colon.inc"
%include "words.inc"

global _start

section .rodata
keyword: db "Введите ключ: ", 10, 0
word_found: db "Совпадение найдено, значение: ", 0
word_not_found: db "Совпадение не найдено", 10, 0
out_of_buffer: db "Введенный ключ длинее 255 символов", 10, 0
newline: db 10

%define buffer_size 255
%define address_size 8

section .bss
buffer: resb buffer_size


section .text
_start:
    mov rdi, keyword
    call print_string
    sub rsp, buffer
    mov rdi, rsp
    call read_line
    cmp rdx, buffer_size
    jg .buffer_overflow

    mov rdi, rax
    mov rsi, head
    call find_word
    test rax, rax
    jz .not_found

    push rax
    mov rdi, word_found
    call print_string
    pop rax

    mov rdi, rax
    add rdi, address_size
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    mov rsi, 1
    call print_string
    mov rdi, newline
    call print_string
    add rsp, buffer
    xor rdi, rdi
    call exit

.not_found:
    mov rdi, word_not_found
    call print_error
    mov rdi, 1
    jmp exit

.buffer_overflow:
    mov rdi, out_of_buffer
    call print_error
    mov rdi, 1
    jmp exit
