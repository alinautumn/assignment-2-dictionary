
all: dictionary

dictionary: main.o dict.o lib.o
		ld -o $@ $^

%.o: %.asm
		nasm -f elf64 $< -o $@

clean:
		rm *.o dictionary

.PHONY: clean
