global find_word

extern string_equals

section .text

find_word:
    cmp rsi, 0
    je .err
    add rsi, 8
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    jz .success
    sub rsi, 8
    mov rsi, [rsi]
    jmp find_word

.err:
    xor rax, rax
    ret

.success:
    sub rsi, 8
    mov rax, rsi
    ret
